<?php

Class checkout{
	function __construct(){


	global $woocommerce;

/* 	$width = WC()->session->get( 'width' );
	$height = WC()->session->get( 'height' );
	$distanceHtml = WC()->session->get( 'distanceHtml' );
	$durationHtml = WC()->session->get( 'durationHtml' );
	$estimated_fare = WC()->session->get( 'estimated_fare' );
	$cartypes = WC()->session->get( 'cartypes' );
	$source = WC()->session->get( 'source' );
	$destination = WC()->session->get( 'destination' );
	$car_seats = WC()->session->get( 'car_seats' );
	$dateTimePickUp = date(getFormatDateTime("php"),strtotime(WC()->session->get( 'dateTimePickUp' )));
	$dateTimePickUpRoundTrip = date(getFormatDateTime("php"),strtotime(WC()->session->get( 'dateTimePickUpRoundTrip' )));
	$nbToll = WC()->session->get( 'nbToll' );
	$suitcases = WC()->session->get( 'suitcases' );

	$stern_taxi_fare_round_trip = (WC()->session->get( 'stern_taxi_fare_round_trip' )=="true") ? __('Round Trip', 'stern_taxi_fare') : __('One way', 'stern_taxi_fare') ;
 */


	//Get data from PHP Sessions
	session_start();  	
	$ins_price_sel = $_SESSION["ins_price"];
	$ins_compname_sel = $_SESSION["ins_compname"];
	$ins_gcc_spec_sel = $_SESSION["ins_gcc_spec"];
	$ins_vehicle_cat_sel = $_SESSION["ins_vehicle_cat"];
	$ins_repair_cond_sel = $_SESSION["ins_repair_cond"];
	$ins_minimum_val_sel = $_SESSION["ins_minimum_val"];
	$ins_maximum_val_sel = $_SESSION["ins_maximum_val"];
	$ins_rate_sel = $_SESSION["ins_rate"];
	$ins_minimum_sel = $_SESSION["ins_minimum"];
	$ins_dl_less_than_year_sel = $_SESSION["ins_dl_less_than_year"];
	$ins_less_than_year_sel = $_SESSION["ins_less_than_year"];
	$ins_os_coverage_sel = $_SESSION["ins_os_coverage"];
	$ins_pa_driver_sel = $_SESSION["ins_pa_driver"];
	$ins_papp_sel = $_SESSION["ins_papp"];
	$ins_rs_assistance_sel = $_SESSION["ins_rs_assistance"];
	$ins_rent_car_sel = $_SESSION["ins_rent_car"];
	$ins_excess_amount_sel = $_SESSION["ins_excess_amount"];

	//User Data
	$ins_reg_date_aj = $_SESSION["ins_reg_date_aj"];
	$ins_polstart_date_aj = $_SESSION["ins_polstart_date_aj"];
	$ins_email_address_aj = $_SESSION["ins_email_address_aj"];
	$ins_firstName_aj = $_SESSION["ins_firstName_aj"];
	$ins_lastname_aj = $_SESSION["ins_lastname_aj"];
	$ins_dob_aj = $_SESSION["ins_dob_aj"];
	$ins_nationality_aj = $_SESSION["ins_nationality_aj"];
	$ins_contact_num_aj = $_SESSION["ins_contact_num_aj"];
	$ins_country_firstdl_aj = $_SESSION["ins_country_firstdl_aj"];
	$ins_long_uaedrivinglicence_aj = $_SESSION["ins_long_uaedrivinglicence_aj"];
	$ins_years_dltotal_aj = $_SESSION["ins_years_dltotal_aj"];
	$ins_imported_aj = $_SESSION["ins_imported_aj"];



	?>

    <br>
	<div id="my_custom_checkout_field">

		<h3><?php _e('Company Details', 'stern_taxi_fare' ); ?></h3>
		<br>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Company Name', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_compname_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Price', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_price_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Support Modified', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_gcc_spec_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Vehicle Category', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_vehicle_cat_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Repair Condition', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_repair_cond_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Driving license less than 1 Year', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_dl_less_than_year_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Age less than 25 year', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_less_than_year_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Out Side UAE Coverage', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_os_coverage_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Personal Accident (Driver)', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_pa_driver_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Personal Accident Per Passengers', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_papp_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Road Side Assistance', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_rs_assistance_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Rant a Car', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_rent_car_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Excess Amount', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_excess_amount_sel; ?>">			
		</p>

		<h3><?php _e('User Details', 'stern_taxi_fare' ); ?></h3>
		<br>
		
		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Registration Date', 'stern_taxi_fare' ); ?></label>
			<input name="ins_regdate" type="text" class="input-text" readonly value="<?php echo $ins_reg_date_aj; ?>">			
		</p>
		
		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Policy Start Date', 'stern_taxi_fare' ); ?></label>
			<input name="ins_polstartdate" type="text" class="input-text" readonly value="<?php echo $ins_polstart_date_aj; ?>">			
		</p>
		
		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Email Address', 'stern_taxi_fare' ); ?></label>
			<input name="ins_email" type="text" class="input-text" readonly value="<?php echo $ins_email_address_aj; ?>">			
		</p>
		
		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'First name', 'stern_taxi_fare' ); ?></label>
			<input name="ins_firstname" type="text" class="input-text" readonly value="<?php echo $ins_firstName_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Last name', 'stern_taxi_fare' ); ?></label>
			<input name="ins_lastname" type="text" class="input-text" readonly value="<?php echo $ins_lastname_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Date of Birth', 'stern_taxi_fare' ); ?></label>
			<input name="ins_dob" type="text" class="input-text" readonly value="<?php echo $ins_dob_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Nationality', 'stern_taxi_fare' ); ?></label>
			<input name="ins_nationality" type="text" class="input-text" readonly value="<?php echo $ins_nationality_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Contact number', 'stern_taxi_fare' ); ?></label>
			<input name="ins_contact_num" type="text" class="input-text" readonly value="<?php echo $ins_contact_num_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Country of Your First Driving Licence', 'stern_taxi_fare' ); ?></label>
			<input name="ins_countryfdl" type="text" class="input-text" readonly value="<?php echo $ins_country_firstdl_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Year Driving in UAE', 'stern_taxi_fare' ); ?></label>
			<input name="ins_years_druae" type="text" class="input-text" readonly value="<?php echo $ins_long_uaedrivinglicence_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Year Driving in Total', 'stern_taxi_fare' ); ?></label>
			<input name="ins_years_dltotal" type="text" class="input-text" readonly value="<?php echo $ins_years_dltotal_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'My Car Imported', 'stern_taxi_fare' ); ?></label>
			<input name="ins_imported" type="text" class="input-text" readonly value="<?php echo $ins_imported_aj; ?>">			
		</p>


    </div>
	<?php
	}
}
