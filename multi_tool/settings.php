<?php
/*
 Plugin Name: Custom post uploader
 Description: This plugin use to import posts from CSV.
 Version: 1.0.0
 Author: Logics Buffer
*/

add_action('admin_menu', 'register_custom_post_uploader' );
function register_custom_post_uploader(){
	add_menu_page( 'Custom post uploader', 'Custom post uploader', 'manage_options', 'custom_post_uploader', 'custom_post_uploader'); 
}
function custom_post_uploader(){
			
		//Upload csv Form		
		?>	
		<h2>Custom Post Uploader</h2>
		<label>Please select upload CSV file then upload</label>
		<form id="featured_upload" method="post" action="#" enctype="multipart/form-data">
			<input type="file" name="my_image_upload" id="my_image_upload"  multiple="false" />
			<input type="hidden" name="post_id" id="post_id" value="0" />
			<?php wp_nonce_field( 'my_image_upload', 'my_image_upload_nonce' ); ?>
			<input id="submit_my_image_upload" name="submit_my_image_upload" type="submit" value="Upload CSV" />
		</form>
		
		
		<?php
		// uploaded data handler
		$post_id = 0;
		/* $post_id22 = 3796;
		$feature_image = 'https://www.ekito.fr/people/wp-content/uploads/2017/01/sample-1.jpg'; */
		
		if ( 
			isset( $_POST['my_image_upload_nonce'] ) 
			&& wp_verify_nonce( $_POST['my_image_upload_nonce'], 'my_image_upload' ))
		 {
			// The nonce was valid and the user has the capabilities, it is safe to continue.

			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			
			// Let WordPress handle the upload.
			// Remember, 'my_image_upload' is the name of our file input in our form above.
			$attachment_id = media_handle_upload( 'my_image_upload',$post_id);
			echo $attachment_id;
			update_option( 'attachment_id_uploadedcsv', $attachment_id,true );
			$id_uploadedcsv_get = get_option( 'attachment_id_uploadedcsv' );
			$uploaded_csvurl = wp_get_attachment_url( $id_uploadedcsv_get );
			update_option( 'uplded_csvurl', $uploaded_csvurl,true );
			echo $uploaded_csvurl;
			
			if ( is_wp_error( $attachment_id ) ) {
				// There was an error uploading the image.
				echo "There was an error uploading the image.";
			} else {
				// The image was uploaded successfully!
				echo "The image was uploaded successfully!";
			}

			} else {
				// The security check failed, maybe show the user an error.
				//echo "The security check failed";
			}
			
			
			//Getting Data from uploaded CSV
			$uploaded_csvurlGet = get_option( 'uplded_csvurl');
			$handle = fopen($uploaded_csvurlGet, "r");
			for ($i = 0; $row = fgetcsv($handle ); ++$i) {
				// Do something will $row array				
				$row1[] = $row;
			}			
			fclose($handle); 			
			update_option( 'frontend_dataComp', $row1,true );
			?><pre><?php //print_r($row1);?></pre><?php		

			?>
			<!-- //Scroll table -->
			
			<div class="wrapper1">
	<div class="div1"></div>
</div>
			<div class="wrapper2">
				<div class="div2">
			
			<table id="show_formdata_comp">
				<thead>
				<tr class="fd-head">
					<td>Title</td>	
					<td>Content of post</td>	
					<td>Featured Image</td>							
					<td>Tags</td>							
					<td>Category</td>							
					<td>Date</td>							
												
				</tr>
				</thead>
				
				
				<?php
				//Shwo data Backend
				$data_showComp = get_option( 'frontend_dataComp' );
				//$data_showComp = json_decode($csv_options, true);
					array_shift($data_showComp);				
					 foreach ( $data_showComp as $get_partsComp ) {
												
						?><tr class="fd-displayinner-comp"><?php
						?> <td> <?php print_r($get_partsComp[0]); ?> </td> <?php				
 						?> <td> <?php print_r($get_partsComp[1]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[2]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[3]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[4]); ?> </td> <?php				
						?> <td> <?php print_r($get_partsComp[5]); ?> </td> <?php 				
						?></tr><?php
						// Create post object
						$post_title = $get_partsComp[0];
						$post_content = $get_partsComp[1];
						$feature_image = $get_partsComp[2];
						$tags = $get_partsComp[3];
						$post_tags = (explode(",",$tags));
						$category = $get_partsComp[4];
						$post_categories = (explode(",",$category));
						$date = $get_partsComp[5];
						$formated_date = date("Y-m-d H:i", strtotime($date));
						$my_post = array(
							'post_title'    => $post_title,
							'post_content'  => $post_content,
							'post_status'   => 'publish',
							'post_date'     => $formated_date,
							'post_author'   => 1
						);
						// Insert the post into the database.

						$post_id = wp_insert_post( $my_post );
						wp_set_object_terms( $post_id, $post_categories, 'category');
						wp_set_object_terms( $post_id, $post_tags, 'post_tag');
						update_post_meta( $post_id, 'fifu_image_url', $feature_image );
		
				} 
				?>
			</table>
			</div>
			</div>
			<!-- Scroll Table End -->
			
			<style>
			.wrapper1, .wrapper2 { width: 100%; overflow-x: scroll; overflow-y: hidden; }
.wrapper1 { height: 20px; }
.wrapper2 {}
.div1 { height: 20px; }
.div2 { overflow: none; }

			</style>	
			
			<script>
			jQuery(function(){
					jQuery('.wrapper1').on('scroll', function (e) {
						jQuery('.wrapper2').scrollLeft(jQuery('.wrapper1').scrollLeft());
					}); 
					jQuery('.wrapper2').on('scroll', function (e) {
						jQuery('.wrapper1').scrollLeft(jQuery('.wrapper2').scrollLeft());
					});
				});
				jQuery(window).on('load', function (e) {
					jQuery('.div1').width(jQuery('table').width());
					jQuery('.div2').width(jQuery('table').width());
				});

			</script>
			
			
			<!-- Scroll Exp-->
			
			
			
			
			
			<style>
			#fd_companiestable td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}

			#show_formdata_comp .fd-head td{
				font-weight: bold;
				border: 2px solid #e5e5e5;
				padding: 10px;
					
			}
			tr.fd-displayinner-comp td {
				text-align: center;
				background: white;
			}
			div#cinfo-tabstp2cont {
				width: 100% !important;
				display: block !important;
				overflow: scroll !important;
				scroll-behavior: smooth !important;
			}
			</style>
			<!--
			<h2>Upload Data Here</h2>
			<table id="fd_companiestable">
			<tr>
					<td>Company Name</td>	
					<td>Support Modified</td>	
					<td>Vehicle Category</td>	
					<td>Repair Condition</td>	
					<td>Min. Value</td>	
					<td>Max. Value</td>	
					<td>Rate</td>
					<td>Final Price(minimum)</td>
					<td>Driving license less than 1 Year</td>
					<td>Age less than 25 year</td>
					<td>Out Side UAE Coverage</td>
					<td>Personal Accident (Driver)</td>
					<td>Personal Accident Per Passengers</td>
					<td>Road Side Assistance</td>
					<td>Rant a Car</td>
					<td>Excess Amount</td>
			<tr/>
			</table>
			<form method="post">
			<textarea cols="201" rows="5" name="fdCompanies" id="fdCompanies"></textarea><br/>
				<input type="submit" id="fdCompaniesSubmit" value="Go" class="button-primary" name="fdCompaniesSubmit" />
			</form>
			-->
			<?php
		}