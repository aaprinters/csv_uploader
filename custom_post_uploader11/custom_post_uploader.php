<?php
/*
Plugin Name: Custom Post Oploader
Description: Custom Post Oploader
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'custom_post_uploader_script_front_css');
add_action('admin_enqueue_scripts', 'custom_post_uploader_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {

}
function custom_post_uploader_script_back_css() {

}

function custom_post_uploader_script_front_css() {
		/* CSS */
        wp_register_style('custom_post_uploader_style', plugins_url('css/geo_my_wp.css',__FILE__));
        wp_enqueue_style('custom_post_uploader_style');
}
